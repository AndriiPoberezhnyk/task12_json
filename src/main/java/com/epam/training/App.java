package com.epam.training;

import com.epam.training.view.View;

import java.util.ResourceBundle;

public class App {
    public static final String JSON_FILE_PATH = ResourceBundle.getBundle("paths")
            .getString("jsonFilePath");

    public static void main(String[] args) {
        new View().run();
    }
}
