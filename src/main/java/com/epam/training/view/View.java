package com.epam.training.view;

import com.epam.training.comparator.VoucherComparator;
import com.epam.training.model.Voucher;
import com.epam.training.parser.GSONParser;
import com.epam.training.parser.JacksonParser;
import com.epam.training.validator.JsonValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.json.stream.JsonParser;
import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private ResourceBundle bundle;

    public View() {
        bundle = ResourceBundle.getBundle("Menu");
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
    }

    public void run(){
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::validateJSON);
        methodsMenu.put("2", this::printJSONAsListByGSON);
        methodsMenu.put("3", this::printJSONAsListByJackson);
        outputMenu();
    }

    private void validateJSON(){
        if (JsonValidator.validate()){
            logger.info("File validation success");
        }else{
            logger.error("File validation failed");
        }
    }

    private void printJSONAsListByGSON(){
        logger.info("<--------------GSON------------->");
        GSONParser gsonParser = new GSONParser();
        List<Voucher> vouchers = gsonParser.readJSON();
        vouchers.sort(new VoucherComparator());
        vouchers.forEach(logger::info);
        logger.info("</-------------GSON------------->");
    }

    private void printJSONAsListByJackson(){
        logger.info("<------------Jackson------------>");
        JacksonParser jacksonParser = new JacksonParser();
        List<Voucher> vouchers = jacksonParser.readJSON();
        vouchers.sort(new VoucherComparator());
        vouchers.forEach(logger::info);
        logger.info("</-----------Jackson------------>");
    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}
