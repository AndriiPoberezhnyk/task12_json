package com.epam.training.comparator;

import com.epam.training.model.Voucher;

import java.util.Comparator;

public class VoucherComparator implements Comparator<Voucher> {
    @Override
    public int compare(Voucher o1, Voucher o2) {
        return o1.getCost().compareTo(o2.getCost());
    }
}
