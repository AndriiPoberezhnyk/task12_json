package com.epam.training.validator;

import com.epam.training.App;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.leadpony.justify.api.JsonSchema;
import org.leadpony.justify.api.JsonValidationService;
import org.leadpony.justify.api.ProblemHandler;

import javax.json.stream.JsonParser;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

public class JsonValidator {
    private static final String jsonFilePath = ResourceBundle.getBundle("paths")
            .getString("schemaPath");
    private static final Logger logger =
            LogManager.getLogger(JsonValidator.class.getName());

    public static boolean validate() {
        AtomicInteger countProblems = new AtomicInteger();
        JsonValidationService service = JsonValidationService.newInstance();
        JsonSchema schema = service.readSchema(Paths.get(jsonFilePath));
        ProblemHandler handler = service.createProblemPrinter(e -> {
            logger.error(e);
            countProblems.getAndIncrement();
        });

        Path path = Paths.get(App.JSON_FILE_PATH);
        try (JsonParser parser = service.createParser(path, schema, handler)) {
            while (parser.hasNext()) {
                parser.next();
            }
        } catch (Exception ignored) {
        }
        return countProblems.get() == 0;
    }
}
