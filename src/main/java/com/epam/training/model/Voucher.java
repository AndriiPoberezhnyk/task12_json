package com.epam.training.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class Voucher implements Comparable<Voucher> {
    private int voucherNumber;
    private String type;
    private String country;
    private int duration;
    private String transport;
    private Hotel hotel;
    private BigDecimal cost;

    public Voucher() {
    }

    public Voucher(int voucherNumber, String type, String country, int duration, String transport, Hotel hotel, BigDecimal cost) {
        this.voucherNumber = voucherNumber;
        this.type = type;
        this.country = country;
        this.duration = duration;
        this.transport = transport;
        this.hotel = hotel;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "voucherNumber=" + voucherNumber +
                ", type='" + type + '\'' +
                ", country='" + country + '\'' +
                ", duration=" + duration +
                ", transport='" + transport + '\'' +
                ", hotel=" + hotel +
                ", cost=" + cost +
                '}';
    }

    @Override
    public int compareTo(Voucher o) {
        return cost.compareTo(o.getCost());
    }
}
