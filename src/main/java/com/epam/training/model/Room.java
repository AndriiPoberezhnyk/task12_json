package com.epam.training.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Room {
    private int size;
    private boolean tv;
    private boolean airCondition;
    private boolean wifi;
    private boolean phone;
    private boolean wardrobe;

    public Room() {
    }

    public Room(int size, boolean tv, boolean airCondition, boolean wifi, boolean phone, boolean wardrobe) {
        this.size = size;
        this.tv = tv;
        this.airCondition = airCondition;
        this.wifi = wifi;
        this.phone = phone;
        this.wardrobe = wardrobe;
    }

    @Override
    public String toString() {
        return "Room{" +
                "size=" + size +
                ", tv=" + tv +
                ", airCondition=" + airCondition +
                ", wifi=" + wifi +
                ", phone=" + phone +
                ", wardrobe=" + wardrobe +
                '}';
    }

    public void isTv(boolean tv) {
        this.tv = tv;
    }

    public void isAirCondition(boolean airCondition) {
        this.airCondition = airCondition;
    }

    public void isWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public void isPhone(boolean phone) {
        this.phone = phone;
    }

    public void isWardrobe(boolean wardrobe) {
        this.wardrobe = wardrobe;
    }
}
