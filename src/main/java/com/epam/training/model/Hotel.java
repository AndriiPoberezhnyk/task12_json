package com.epam.training.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hotel {
    private int stars;
    private String food;
    private Room room;

    public Hotel() {
    }

    public Hotel(int stars, String food, Room room) {
        this.stars = stars;
        this.food = food;
        this.room = room;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "stars=" + stars +
                ", food='" + food + '\'' +
                ", room=" + room +
                '}';
    }
}
