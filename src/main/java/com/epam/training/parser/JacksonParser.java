package com.epam.training.parser;

import com.epam.training.App;
import com.epam.training.model.Voucher;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.List;

public class JacksonParser {

    public List<Voucher> readJSON() {
        List<Voucher> vouchers = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            vouchers = mapper.readValue(new File(App.JSON_FILE_PATH),
                    new TypeReference<List<Voucher>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vouchers;
    }
}
