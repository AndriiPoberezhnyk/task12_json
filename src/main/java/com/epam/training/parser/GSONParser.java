package com.epam.training.parser;

import com.epam.training.App;
import com.epam.training.model.Voucher;
import com.google.gson.Gson;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GSONParser {

    public List<Voucher> readJSON() {
        Gson gson = new Gson();
        List<Voucher> vouchers = null;
        try (Reader reader = new FileReader(App.JSON_FILE_PATH)) {
            Voucher[] voucherArr = gson.fromJson(reader, Voucher[].class);
            vouchers = new ArrayList<>(Arrays.asList(voucherArr));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vouchers;
    }
}
